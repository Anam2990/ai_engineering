<img src="images/IDSNlogo.png" width = "300">

# Objective

After completing this lab, you will be able to:

1. Add a Watson Studio - Lite service
2. Create a project in Watson Studio
3. Add a notebook to a project
4. Perform Project
5. Share your results

# Setup Watson Studio

_If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:_

## Step 1: For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

[Click here](https://cloud.ibm.com/catalog/services/watson-studio)

You will see the screen in the figure below. Click the icon in the red box:

<img src="images/1.png" width = "600">

Then click **Watson**, as shown below:

<img src="images/2.png" width = "600">

Then click **Browse Services**.

<img src="images/3.png" width = "600">


Scroll down and select **Watson Studio - Lite.**

<img src="images/4.png" width = "600">


To create a Watson service using the Lite plan, click **Create**.

<img src="images/5.png" width = "600">

Now click **Get Started**.

<img src="images/6.png" width = "600">

After creating the service continue with Step 2.

## Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services**.

<img src="images/7.png" width = "600">


When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

<img src="images/8.png" width = "600">

Then click **Get Started**.

<img src="images/9.png" width = "600">

## Step 3: Creating a Project

Now you have to Create a project.

Click on **Create a project**:

<img src="images/10.png" width = "600">


On the Create a project page, click **Create an empty project**

<img src="images/11.png" width = "600">

Provide a **Project Name** and **Description**, as shown below:

<img src="images/12.png" width = "600">

You must also create storage for the project.
Click **Add**

<img src="images/13.png" width = "600">

On the Cloud Object Storage page, scroll down and then click **Create**.

<img src="images/14.png" width = "600">

In the Confirm Creation box, click **Confirm**.

<img src="images/15.png" width = "300">

On the New project page, note that the storage has been added, and then click **Create**.

<img src="images/16.png" width = "600">

After creating the project continue with Step 3.

## Step 3: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project**.

<img src="images/17.png" width = "600">


In the list of asset types, click **Notebook**:

<img src="images/18.png" width = "600">

On the New Notebook page, enter a name for the notebook, and then click **From URL**.
Copy this link:

[Click here](https://cocl.us/Coursera_DL0110EN_Honors)

Paste it into the **Notebook URL** box, and then click **Create Notebook**.

<img src="images/19.png" width = "600">

You will see this Notebook:

<img src="images/20.png" width = "600">


Once you complete your notebook you will have to share it. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all content excluding sensitive code cells.

<img src="images/21.png" width = "600">

<img src="images/21.2.png" width = "600">

<img src="images/22.png" width = "600">

## Author(s)
[Joseph Santarcangelo](https://www.linkedin.com/in/joseph-s-50398b136/)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-17 | 2.0 | Shubham | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |


