<img src="images/IDSNlogo.png" width = "300">

# Setup Watson Studio

## Objective

After completing this lab, you will be able to:

1. Add a Watson Studio - Lite service
2. Create a project in Watson Studio
3. Add a notebook to a project
4. Perform Project Pre-trained Model
5. Share your results

## Scenario

For the deep learning portion of this lab, we will use Watson Studio. Its a more powerful version of Skills Network labs. It will also allow you to share your notebook to be marked. You will need the link from your Jupyter notebook from the previous section.

You will use the Jupyter notebook for your Particular Deep learning Framework. You will complete the notebook and submit it along with several screenshots to be marked by your peers.

The following will be the instructions on how to sign up for an account, load the notebook and share it. If you already have an account Please jump to Task two.

## Exercise 1: Add a Watson Studio - Lite service

### Scenario

In this exercise, you will use the IBM Cloud account you configured in the previous module. You will add the Watson Studio - Lite service to your IBM Cloud account.

If you have already added a Watson Studio - Lite service, you can skip Task 1 and proceed with Task 2.

### Task 1: Log in to your IBM Cloud account and add the Watson Studio - Lite service

_Complete this task only if you have not yet configured Watson Studio - Lite. Otherwise, go to Task 2_.

1. Go to the [IBM Cloud](https://cloud.ibm.com/) page, enter your **ID**, and then click **Continue**.


<img src="images/1.png" width = "600">

2. On the Dashboard, click **Create Resource**


<img src="images/2.png" width = "600">

3. In the Catalog, click **AI (16)**. Note that the **Lite** plan is selected

<img src="images/3.png" width = "600">

4. In the list of **Services**, click **Watson Studio**.

<img src="images/4.png" width = "600">

5. On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create**.


<img src="images/5.png" width = "600">

6. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**.

<img src="images/6.png" width = "600">

7. You will see this message when Watson Studio is successfully set up for you. Click **Get Started**.

<img src="images/7.png" width = "600">

## Task 2: Launch Watson Studio

_Complete this task if you have an existing Watson Studio - Lite service. Otherwise, go to Task 1_.

1. Go to the [IBM Cloud](https://cloud.ibm.com/) page, enter your **ID**, and then click **Continue**.

<img src="images/8.png" width = "600">

2. On the Dashboard, click **Services**.

<img src="images/9.png" width = "600">

3. In the Resource list, expand **Services**, and then click the Watson Studio service.

<img src="images/10.png" width = "600">

4. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**

<img src="images/11.png" width = "600">

## Exercise 2: Create a new project and add a Jupyter Notebook

### Scenario

In this exercise, you will create a project to hold all the resources and services for your data analysis.

### Task 1: Create a new project

1. On the Watson Studio Welcome page, click **Create a project**.

<img src="images/12.png" width = "600">

2. On the Create a project page, click **Create an empty project**

<img src="images/13.png" width = "600">

3. On the New project page, enter a **Name** and **Description** for your project

<img src="images/14.png" width = "600">

4. If your IBM Cloud account does not have existing storage for your project, you will be prompted to create it.
   If your IBM Cloud account does have storage, go to _Task 2: Add a Jupyter Notebook_.
   Under Select **storage service**, click **Add**.

<img src="images/15.png" width = "600">

5. On the Cloud Object Storage page, verify that **Lite** is selected, and then click **Create**.

<img src="images/16.png" width = "600">

6. In the Confirm Creation box, click **Confirm**.

<img src="images/17.png" width = "600">

## Task 2: Add a Jupyter Notebook

1. On the project page, click **Add to project**.

<img src="images/18.png" width = "600">

2. In the Choose asset type box, click **Notebook**

<img src="images/19.png" width = "600">

3. On the Load Notebook page, enter a **Name** for your notebook, then click **From URL**. On the **Notebook URL** line, copy and  paste the notebook URL from the introduction for the deep learning framework of yourchoice.


<img src="images/20.png" width = "600">

4. The notebook you select, wwill contains the instructions and information for the assignment, is loaded

<img src="images/21.png" width = "600">

## Exercise 3: Perform Project on Pre-trained Model

### Scenario
Train a neural network to determine if an image of concrete has a crack or does not have a crack.

### Task 1: Train the output layer

1. Follow the instructions in the Notebook to complete the assignment.

### Task 2: Share your results

1. In the Notebook, on the toolbar, click **Share**

<img src="images/22.png" width = "600">

2. In the Share box, select **All content excluding sensitive code cells**.

<img src="images/23.png" width = "600">

3. To share the Notebook, scroll down and copy the permalink.

<img src="images/24.png" width = "600">

## Author(s)
[Joseph Santarcangelo](https://www.linkedin.com/in/joseph-s-50398b136/)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-17 | 2.0 | Shubham | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |
